package fr.atchisson.musculAction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import java.util.ArrayList;

public class main extends Activity {
    /**
     * Called when the activity is first created.
     */
    ArrayList<Entrainement> entrainements;
    ArrayAdapter<Entrainement> adapter;
    openHelper db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        entrainements = new ArrayList<Entrainement>();
        db = new openHelper(this);
        entrainements = db.get_entrainements();
        ListView lv = (ListView)findViewById(R.id.listView);
        adapter = new ArrayAdapter<Entrainement>(this, android.R.layout.simple_list_item_1, entrainements);
        lv.setAdapter(adapter);
        View.OnClickListener handler_new_entrainement = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEntrainement();
            }
        };
        Button btn = (Button)findViewById(R.id.btn_new_entrainement);
        btn.setOnClickListener(handler_new_entrainement);
        /*lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                entrainements.remove(pos);
                maj_DB(true);
                maj_liste();
                return true;
            }
        });*/
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                afficheEntrainement(position);
                //Toast.makeText(MyActivity.this, "on ajoute", Toast.LENGTH_SHORT);
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.resetBD:
                db.drop_all();
                db.onCreate();
                maj_liste();
                return true;

            case R.id.stats:
                startIntentStats();
                return true;

            case R.id.gallery:
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addEntrainement() {
        newEntrainementActivity();
        maj_liste();
    }

    public void maj_liste() {
        entrainements = db.get_entrainements();
        ListView lv = (ListView)findViewById(R.id.listView);
        adapter = new ArrayAdapter<Entrainement>(this, android.R.layout.simple_list_item_1, entrainements);
        lv.setAdapter(adapter);
    }

    public void newEntrainementActivity() {
        Intent intent = new Intent(this, nouvel_entrainement.class);
        startActivityForResult(intent, 1);
    }

    public void startIntentStats() {
        Intent intent = new Intent(this, stats.class);
        startActivity(intent);
    }

    public void afficheEntrainement(int i) {
        maj_DB(false);

        //démarrer l'activité
        Intent intent = new Intent(this, affichage_entrainement.class);
        Entrainement e = entrainements.get(i);
        Log.d("coucou", "affichage entrainement : id = "+e.id);

        intent.putExtra("id_entrainement", e.id);
        intent.putExtra("title", e.title);
        intent.putExtra("date", e.date);
        startActivity(intent);
    }

    private void maj_DB(boolean verifdel) {
        db.sync(entrainements, verifdel);
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent intent){
        switch (requestCode) {
            case 1: //nouvel entrainement
                maj_liste();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        db.sync(entrainements, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        maj_liste();
    }
}