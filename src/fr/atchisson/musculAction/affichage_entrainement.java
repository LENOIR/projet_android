package fr.atchisson.musculAction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.*;

import java.util.ArrayList;

/**
 * Created by atchisson on 10/03/2016.
 */
public class affichage_entrainement extends Activity {
    //appel de l'intent lors d'un clic cours sur l'item

    Entrainement entrainement;
    ArrayList<Exercice> exos;
    ArrayAdapter<Exercice> adapter;
    openHelper db;
    int id;

    @Override
    public void onCreate(Bundle s) {
        super.onCreate(s);
        setContentView(R.layout.affichage_entrainement);

        db = new openHelper(this);
        Bundle bundle = getIntent().getExtras();
        id = bundle.getInt("id_entrainement");
        openHelper db = new openHelper(this);
        exos = db.get_exos(id);
        Log.d("coucou", "nombre d'exo = "+db.get_exos(id).size());
        entrainement = new Entrainement();
        entrainement.id = id;
        entrainement.title = bundle.getString("title");
        entrainement.date = bundle.getString("date");
        adapter = new ArrayAdapter<Exercice>(this, android.R.layout.simple_list_item_1, exos);
        adapter.setNotifyOnChange(true);
        ListView lv = (ListView)findViewById(R.id.lv_exercices);
        lv.setAdapter(adapter);

        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        TextView tv_date = (TextView) findViewById(R.id.tv_date);

        tv_title.setText(entrainement.title);
        tv_date.setText(entrainement.date);

        View.OnClickListener handler_new_entrainement = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newExercice();
            }
        };
        Button btn = (Button)findViewById(R.id.new_exo);
        btn.setOnClickListener(handler_new_entrainement);
    }

    @Override
    public void finish() {
        super.finish();
    }

    public void maj() {
        openHelper db = new openHelper(this);
        db.maj_entrainement(entrainement);
    }

    private void newExercice() {
        Intent intent = new Intent(this, nouvel_exercice.class);
        intent.putExtra("id_entrainement", entrainement.id);
        Log.d("coucou", "ajout d'un exercice sur l'entrainement "+  entrainement.id);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent intent){
        switch (requestCode) {
            case 1: //nouvel exercice
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                exos = null;
                adapter.notifyDataSetChanged();
                exos = db.get_exos(id);
                adapter.notifyDataSetChanged();
                break;
            default:
                break;
        }
    }
}
