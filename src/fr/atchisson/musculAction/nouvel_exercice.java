package fr.atchisson.musculAction;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by atchisson on 23/03/2016.
 */
public class nouvel_exercice extends Activity {
    Exercice exo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nouvel_exercice);

        exo = new Exercice();
        Bundle bundle = getIntent().getExtras();
        exo.id_entrainement = bundle.getInt("id_entrainement");
        Log.d("coucou", "nouvel exo sur l'entrainement "+exo.id_entrainement);

        View.OnClickListener handler_sauvegarder = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sauvegarder();
            }
        };
        Button btn = (Button)findViewById(R.id.btn_validerNouvelExo);
        btn.setOnClickListener(handler_sauvegarder);
    }

    public void sauvegarder() {
        try {exo.duree    = Integer.parseInt(((EditText) findViewById(R.id.et_duree)).getText().toString());} catch(Exception e) {Toast.makeText(nouvel_exercice.this, "erreur de parsing", Toast.LENGTH_SHORT).show();}
        try {exo.nb_reps  = Integer.parseInt(((EditText) findViewById(R.id.et_reps)).getText().toString());} catch(Exception e) {Toast.makeText(nouvel_exercice.this, "erreur de parsing", Toast.LENGTH_SHORT).show();}
        try {exo.nb_serie = Integer.parseInt(((EditText) findViewById(R.id.et_serie)).getText().toString());} catch(Exception e) {Toast.makeText(nouvel_exercice.this, "erreur de parsing", Toast.LENGTH_SHORT).show();}
        exo.nom      = ((EditText) findViewById(R.id.et_nom)).getText().toString();
        openHelper db = new openHelper(this);
        db.ajout_exercice(exo);
        super.finish();
    }
}
