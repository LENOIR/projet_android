package fr.atchisson.musculAction;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by lenoir on 09/03/16.
 */
public class Entrainement {
    public String date;
    public String title;
    public int id;

    public Entrainement() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
        Calendar calendar = new GregorianCalendar(2013,0,31);
        date = sdf.format(calendar.getTime());
        title = "entrainement du "+date;
    }

    public Entrainement(String s) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
        Calendar calendar = new GregorianCalendar(2013,0,31);
        date = sdf.format(calendar.getTime());
        title = s;
    }

    public String toString() {
        return title;
    }

    public String toStringDebug() {
        return "<Entrainement> id : "+id+" title : "+title+" ";
    }
}
