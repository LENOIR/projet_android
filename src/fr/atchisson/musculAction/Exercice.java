package fr.atchisson.musculAction;

/**
 * Created by lenoir on 09/03/16.
 */
public class Exercice {
    public int id_entrainement, duree, nb_serie, nb_reps, id;
    public String nom, type;

    public Exercice() {}

    public Exercice(int id_entrainement, int duree, int nb_serie, int nb_reps) {
        this.id_entrainement = id_entrainement;
        this.duree = duree;
        this.nb_serie = nb_serie;
        this.nb_reps = nb_reps;
        nom = "exercice de "+duree/60+" minutes";
    }

    public Exercice(int id_entrainement, int duree, int nb_serie, int nb_reps, String nom, String type) {
        this.id_entrainement = id_entrainement;
        this.duree = duree;
        this.nb_serie = nb_serie;
        this.nb_reps = nb_reps;
        this.nom = nom;
        this.type = type;
    }

    public String toString() {
        return nom+" d'une durée de "+duree+" minutes";
    }

    public String toStringDebug() {
        return "<Exercice> id : "+id+" id_entrainement : "+id_entrainement+" nom : "+nom+" type : "+type;
    }
}
