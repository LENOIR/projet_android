package fr.atchisson.musculAction;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by lenoir on 09/03/16.
 */
public class openHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "db";
    private SQLiteDatabase DB;

    public openHelper(Context c) {
        super(c, DB_NAME, null, 1);
        DB = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table entrainement (id integer primary key autoincrement, date integer, title text)");
        db.execSQL("create table exercice (id integer primary key autoincrement, entrainement integer, duree integer, nb_reps integer, nb_serie integer, title text, type text)");
    }

    public void onCreate() {
        DB.execSQL("create table entrainement (id integer primary key autoincrement, date text, title text)");
        DB.execSQL("create table exercice (id integer primary key autoincrement, entrainement integer, duree integer, nb_reps integer, nb_serie integer, title text, type text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
    }

    public void ajout_entrainement(Entrainement e) {
        ContentValues cv = new ContentValues();
        cv.put("date", e.date);
        cv.put("title", e.title);
        DB.insert("entrainement", null, cv);
    }

    public ArrayList<Entrainement> get_entrainements() {
        ArrayList<Entrainement> res = new ArrayList<Entrainement>();
        String[] cols = {"id", "date", "title"};

        Cursor c = DB.query("entrainement", cols, null, null, null, null, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                Entrainement e = new Entrainement();
                e.id = c.getInt(0);
                e.date = c.getString(1);
                e.title = c.getString(2);
                res.add(e);
                c.moveToNext();
            }
            catch (Exception e) {c.moveToNext();}
        }
        c.close();
        return res;
    }

    public Entrainement get_entrainement(int id){
        String query = "select * from entrainement where id="+id;
        Cursor c = DB.rawQuery(query, null);

        Entrainement res = new Entrainement();

        if (c.moveToFirst()) {
            res.date = c.getString(1);
            res.id = c.getInt(0);
            res.title = c.getString(2);
        }
        c.close();

        return res;
    }

    public void maj_entrainement(Entrainement e) {
        ContentValues cv = new ContentValues();
        cv.put("date", e.date);
        cv.put("title", e.title);
        DB.update("entrainement", cv, "id="+e.id, null);
        for (Entrainement ent: get_entrainements()) {
            Log.d("coucou", ent+"");
        }
    }

    public int getIdEntrainement(String title, String date) {
        for(Entrainement e : get_entrainements()) {
            if(e.title == title && e.date == date) {
                return e.id;
            }
        }
        return -1;
    }

    public void drop_all() {
        DB.execSQL("drop table entrainement");
        DB.execSQL("drop table exercice");
    }

    public void ajout_exercice(Exercice e) {
        //(id integer primary key autoincrement, entrainement integer, duree integer, nb_reps integer, nb_serie integer, title text, type text)
        ContentValues cv = new ContentValues();
        try {
            cv.put("id", Integer.parseInt(  (int)(Math.random() * Integer.MAX_VALUE)+""   ));
            cv.put("entrainement", Integer.parseInt(e.id_entrainement + ""));
            cv.put("duree", Integer.parseInt(e.duree + ""));
            cv.put("nb_reps", Integer.parseInt(e.nb_reps + ""));
            cv.put("nb_serie", Integer.parseInt(e.nb_serie + ""));
            cv.put("title", e.nom);
            cv.put("type", e.type);
        }
        catch (Exception err) {Log.e("coucou", err.toString());}
        try {DB.insertOrThrow("exercice", null, cv); } catch (Exception err) {Log.e("coucou", err.toString());}
        Log.d("coucou", "sauvegarde de l'exo "+e.toStringDebug());
    }

    public ArrayList<Exercice> get_exos() {
        ArrayList<Exercice> res = new ArrayList<Exercice>();
        String[] cols = {"id", "entrainement", "duree", "nb_reps", "nb_serie", "title", "type"};

        Cursor c = DB.query("exercice", cols, null, null, null, null, null);
        Log.d("coucou", c.getCount()+" exercices en base");
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                Exercice e = new Exercice();
                e.id = c.getInt(0);
                e.id_entrainement = c.getInt(1);
                e.duree = c.getInt(2);
                e.nb_reps = c.getInt(3);
                e.nb_serie = c.getInt(4);
                e.nom = c.getString(5);
                e.type = c.getString(6);
                res.add(e);
                c.moveToNext();
            }
            catch (Exception e) {c.moveToNext();e.printStackTrace();Log.e("coucou", e.toString());}
        }
        c.close();
        return res;
    }

    public ArrayList<Exercice> get_exos(int id_entrainement) {
        ArrayList<Exercice> liste = get_exos();
        ArrayList<Exercice> res = new ArrayList<Exercice>();
        for(Exercice e : liste) {
            if (e.id_entrainement == id_entrainement) {
                res.add(e);
            }
        }
        return res;
    }

    public void log() {
        Log.d("coucou", "dump DB");
        Log.d("coucou", get_entrainements().size()+" entrainements en base");
        for(Entrainement e : get_entrainements()) {
            Log.d("coucou", e.toStringDebug());
        }
        for(Exercice e : get_exos()) {
            Log.d("coucou", e.toStringDebug());
        }
    }

    public void sync(ArrayList<Entrainement> l_new, boolean verifDel) {
        //prend une liste d'entrainements, et si y'en a qui existe pas déjà, l'ajoute a la base
        ArrayList<Entrainement> l_current = get_entrainements();
        for(Entrainement e : l_new) {
            if ( ! entrainementExiste(e, l_current)) {
                ajout_entrainement(e);
            }
        }
        //enleve les entrainements qui n'existent plus
        if(verifDel) {
            for (Entrainement e : l_current) {
                if (!entrainementExiste(e, l_new)) ;
                {
                    Log.d("coucou", "entrainement n'existe plus");
                    deleteEntrainement(e);
                }
            }
        }

    }

    public boolean entrainementExiste(Entrainement e, ArrayList<Entrainement> l) {
        for (int i=0;i<l.size();i++) {
            if (e.id == l.get(i).id) {
                return true;
            }
        }
        return false;
    }

    public void deleteEntrainement(Entrainement e) {
        int nb = DB.delete("entrainement", "id = "+e.id, null);
        Log.d("coucou", nb+" entrainements supprimés");
    }
}
