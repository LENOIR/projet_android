package fr.atchisson.musculAction;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import org.w3c.dom.Text;

/**
 * Created by atchisson on 30/03/2016.
 */
public class stats extends Activity {
    openHelper db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats);

        db = new openHelper(this);

        TextView tv = (TextView)findViewById(R.id.tv_descriptionStats);
        tv.setText("Vous avez effectué "+getTempsTotal()+" minutes d'exercice au total, réparties sur "+getNbEntrainements()+" entrainements !");
    }

    private int getTempsTotal() {
        int res = 0;
        for(Exercice e : db.get_exos()) {
            res += e.duree;
        }
        return res;
    }

    private int getNbEntrainements() {
        return db.get_entrainements().size();
    }
}
