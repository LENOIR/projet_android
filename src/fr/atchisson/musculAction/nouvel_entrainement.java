package fr.atchisson.musculAction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by atchisson on 21/03/2016.
 */
public class nouvel_entrainement extends Activity {
    Entrainement entrainement;
    EditText title;
    TextView tv_date;
    openHelper db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nouvel_entrainement);

        entrainement = new Entrainement();

        db = new openHelper(this);

        title = (EditText)findViewById(R.id.title);
        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
        Date date = new Date();
        title.setText("entrainement du "+dateFormat.format(date));

        tv_date = (TextView)findViewById(R.id.date);
        tv_date.setText(dateFormat.format(date));

        View.OnClickListener handler_sauvegarder = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sauvegarder();
                finish();
            }
        };
        Button btn = (Button)findViewById(R.id.button2);
        btn.setOnClickListener(handler_sauvegarder);
    }

    private void sauvegarder() {
        entrainement.title = title.getText().toString();
        entrainement.date = tv_date.getText().toString();
        db.ajout_entrainement(entrainement);
    }

    public void finish() {
        super.finish();
    }
}